const rowInputFields = [
    {
        key: 'tableID',
        label: 'Table ID',
        type: 'integer',
        required: true,
        altersDynamicFields: true,
    },
]
module.exports = { rowInputFields }